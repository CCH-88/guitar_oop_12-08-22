public class ElectricGuitar extends Guitar {

    String pickUp;

    public ElectricGuitar(String brand, String model, Integer yearManufactured, String pickUp) {
        super.setBrand(brand);
        super.setModel(model);
        super.setYearManufactured(yearManufactured);
    }

    @Override
    public void makeSound() {
        super.makeSound();
        System.out.println("Dewww deww");
    }
}
