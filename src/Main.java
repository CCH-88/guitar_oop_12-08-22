import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {


        ArrayList<Guitar> guitars = new ArrayList<>();
        guitars.add(new AccusticGuitar("Yamaha","LS36",2020));
        guitars.add(new AccusticGuitar("Guild", "F-55E", 1983));
        guitars.add(new ElectricGuitar("Fender", "Stratocaster", 1966,"a pickup"));
        guitars.add(new ElectricGuitar("Epiphone", "Prophecy", 2005,"a pickup"));

        for (int i=0; i<guitars.size(); i++){
            guitars.get(i).makeSound();
        }






    }
}