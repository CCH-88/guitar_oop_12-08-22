public class AccusticGuitar extends Guitar{

    public AccusticGuitar(String brand, String model, Integer yearManufactured) {
        super.setBrand(brand);
        super.setModel(model);
        super.setYearManufactured(yearManufactured);
    }

    @Override
        public void makeSound() {
            super.makeSound();
            System.out.println("ding ding");
        }

}
