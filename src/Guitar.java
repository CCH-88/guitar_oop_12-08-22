public abstract class Guitar implements Playable{

    String brand;
    String model;
    Integer yearManufactured;

    public Guitar(){}


    @Override
    public void makeSound() {
        Playable.super.makeSound();
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public void setYearManufactured(Integer yearManufactured) {
        this.yearManufactured = yearManufactured;
    }

    public Integer getYearManufactured() {
        return yearManufactured;
    }

    public String getBrand() {
        return brand;
    }

    public String getModel() {
        return model;
    }
}
